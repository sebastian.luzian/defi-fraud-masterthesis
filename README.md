# DeFi-Fraud-MasterThesis

In this thesis we investigate money flows from fraudulent entities connected to the DeFi Space.
The setup can be split int 5 stages:
- Preparing fraudulent address data and DeFi data for a Network Import to Neo4j
- - Preparing data for a 1-hop Network Import to Neo4j (optional)
- Network study
- Computing transaction analysis and activity over time
- Computing trace analysis

## Setup - Needed Libraries
    pip install typer
    pip install requests
    pip install retrying
    pip install pyyaml
    pip install pandas
    pip install numpy
    pip install matplotlib

## Folder Structure

```
├── defi-fraud-masterthesis             Project Root Folder
│   ├── cypher_queries                  Folder with cypher queries
│   ├── data                            
│   │   ├── malicious_addresses         Folder with seed data of malicious addresses
│   │   │    ├── input                  Seed data
│   │   │    ├── output                 Output computed by converter
│   │   │    └── converter.py    
│   │   ├── network                     Folder for setting up the full Neo4j network
│   │   │    ├── input                  Seed data
│   │   │    ├── output                 Output computed by converter
│   │   │    └── converter.py  
│   │   ├── one_hop_network             Folder for setting up a 1-hop Neo4j network
│   │   │    ├── input                  Seed data
│   │   │    ├── output                 Output computed by converter
│   │   │    └── converter.py  
│   │   ├── protocols                   Folder with seed data of DeFi protocols
│   │   │    ├── input                  Seed data
│   │   │    ├── output                 Output computed by converter
│   │   │    └── converter.py
│   │   ├── temp                        Folder with temporary data (for transaction activity computations)
│   │   │    ├── input                  Seed data
│   │   │    ├── output                 Output computed by main.py
│   │   │    ├── api.py                 Etherscan api  
│   │   │    └── main.py                Main script for computing the transaction activity over time and plotting results  
│   │   └── traces                      Folder with data for trace analysis
│   │        ├── input                  Seed data
│   │        ├── output                 Output computed by converter
│   │        └── converter.py            
│   └── notes                           notes
└── README.md
```

## Network Setup

In this Section we describe the steps to reproduce the Setup of the Neo4j Network and populate it with the seed data.

### Converting the Malicious Addresses Seed Data

Inside `data/malicious_addresses/converter.py` we provide the following commands:
```
convert-labelcloud-csv
convert-etherscamdb-tagpack-yaml
convert-scams-yaml
convert-addresses-darklist-json
convert-addresses-json
convert-urls-yaml
convert-uris-yaml
convert-all
lowercase
uniqueify
```

Usage:  `converter.py [OPTIONS] COMMAND [ARGS]...`
e.g.: `python converter.py convert-all`

To convert the malicious addresses data we run the follwing commands<br>
(in the location of converter.py, in below order):

```
python converter.py convert-all
python converter.py lowercase
python converter.py uniqueify
```

It creates 2 files inside the [data/malicious_addresses/output](data/malicious_addresses/output) directory:
- [addresses.csv](data/malicious_addresses/output/addresses.csv) and<br>
- [unique_addresses.csv](data/malicious_addresses/output/unique_addresses.csv)<br>

We will need the [unique_addresses.csv](data/malicious_addresses/output/unique_addresses.csv) file to enrich the network data later on.


### Converting Protocols Seed Data

Inside `data/protocols/converter.py` we provide the following command:
```
convert
```

Usage:  `converter.py [OPTIONS] COMMAND [ARGS]...`
e.g.: `python converter.py convert`

To convert the defi protocols data we run the follwing commands<br>
(in the location of converter.py, in below order):<br>
```
python converter.py convert
```

It creates 1 file inside the [data/protocols/output](data/protocols/output) directory:
- [protocols.csv](data/protocols/output/protocols.csv)

This is the second file we need to enrich the network data.

### Enrichment of Network Data

Under the [data/network](data/network) directory we provide another converter to enrich the 
extracted Ethereum network data from CSH with the properties of the fraudulent addresses, 
as well as the properties of the DeFi addresses.
[data/network/input](data/network/input).

The converter `network\converter.py` provides the following commands
```
prepare addresses
prepare_address_relations
```

By running both commands
```
python converter.py prepare addresses
python converter.py prepare_address_relations
```
we populate the 2 folders

- [data/network/output/addresses](data/network/output/addresses)
- [data/network/output/address_relations](data/network/output/address_relations)

which are needed to execute the import to the Neo4J database.

The folders also contain 2 header files needed for the database import:
- [addresses/header.csv](data/network/output/addresses/header.csv)
- [address_relations/header.csv](data/network/output/address_relations/header.csv)

### Neo4J Database Import
First download the Neo4J Desktop Toolkit at https://neo4j.com/download/?ref=try-neo4j-lp.

After we have installed Neo4J and prepared the files that are neccessary for the database import
(located under [data/network/output](data/network/output)) we can now run the import to Neo4J via the neo4j-admin tool:


### Neo4J Locations:
Import Folder:<br>
`C:\Users\USERNAME\.Neo4jDesktop\relate-data\dbmss\dbms-0a6a1b21-1c92-4784-9582-01b9ed5dd0c5\import`<br>

Neo4J Admin Tool Location:<br>
`C:\Users\USERNAME\.Neo4jDesktop\relate-data\dbmss\dbms-0a6a1b21-1c92-4784-9582-01b9ed5dd0c5\bin` 

Note: the hashy locations changes whenever a new DBMS is created!

### Steps to import data with neo4j-admin
1. Create a new Project
2. Add DBMS - Local DBMS with some password e.g. "password"
3. The database we want to create must not exist yet
4. Stop the DBMS
5. Run the one-line-command from below in the Neo4J Admin Tool location<br>(MUST BE ONE LINE ONLY + REPLACE PATH WITH CORRECT FILE LOCATIONS!)<br>
   One-Line-Converter: https://lingojam.com/TexttoOneLine

       neo4j-admin import --database defi --nodes=Address="C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\addresses\header.csv,C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\addresses\output-000001.csv,C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\addresses\output-000002.csv,C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\addresses\output-000003.csv,C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\addresses\output-000004.csv,C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\addresses\output-000005.csv,C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\addresses\output-000006.csv,C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\addresses\output-000007.csv,C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\addresses\output-000008.csv,C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\addresses\output-000009.csv,C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\addresses\output-000010.csv,C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\addresses\output-000011.csv,C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\addresses\output-000012.csv,C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\addresses\output-000013.csv,C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\addresses\output-000014.csv,C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\addresses\output-000015.csv,C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\addresses\output-000016.csv,C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\addresses\output-000017.csv,C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\addresses\output-000018.csv,C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\addresses\output-000019.csv,C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\addresses\output-000020.csv,C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\addresses\output-000021.csv,C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\addresses\output-000022.csv,C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\addresses\output-000023.csv,C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\addresses\output-000024.csv,C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\addresses\output-000025.csv,C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\addresses\output-000026.csv,C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\addresses\output-000027.csv,C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\addresses\output-000028.csv" --relationships=TRANSACTIONS="C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\address_relations\header.csv,C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\address_relations\output-000001.csv,C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\address_relations\output-000002.csv,C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\address_relations\output-000003.csv,C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\address_relations\output-000004.csv,C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\address_relations\output-000005.csv,C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\address_relations\output-000006.csv,C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\address_relations\output-000007.csv,C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\address_relations\output-000008.csv,C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\address_relations\output-000009.csv,C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\address_relations\output-000010.csv,C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\address_relations\output-000011.csv,C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\address_relations\output-000012.csv,C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\address_relations\output-000013.csv,C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\address_relations\output-000014.csv,C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\address_relations\output-000015.csv,C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\address_relations\output-000016.csv,C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\address_relations\output-000017.csv,C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\address_relations\output-000018.csv,C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\address_relations\output-000019.csv,C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\address_relations\output-000020.csv,C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\address_relations\output-000021.csv,C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\address_relations\output-000022.csv,C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\address_relations\output-000023.csv,C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\address_relations\output-000024.csv,C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\address_relations\output-000025.csv,C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\address_relations\output-000026.csv,C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\address_relations\output-000027.csv,C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\address_relations\output-000028.csv"

6. Start the DBMS
7. Inside Neo4J Browser run `:use system` and then `create database defi`
8. Stop the DBMS
9. Start the DBMS
10. Done

Import command from above in readable format (only use the one-line-command from above for import):
```
neo4j-admin import --database defi --nodes=Address="
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\addresses\header.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\addresses\output-000001.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\addresses\output-000002.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\addresses\output-000003.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\addresses\output-000004.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\addresses\output-000005.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\addresses\output-000006.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\addresses\output-000007.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\addresses\output-000008.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\addresses\output-000009.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\addresses\output-000010.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\addresses\output-000011.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\addresses\output-000012.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\addresses\output-000013.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\addresses\output-000014.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\addresses\output-000015.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\addresses\output-000016.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\addresses\output-000017.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\addresses\output-000018.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\addresses\output-000019.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\addresses\output-000020.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\addresses\output-000021.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\addresses\output-000022.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\addresses\output-000023.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\addresses\output-000024.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\addresses\output-000025.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\addresses\output-000026.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\addresses\output-000027.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\addresses\output-000028.csv
" --relationships=TRANSACTIONS="
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\address_relations\header.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\address_relations\output-000001.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\address_relations\output-000002.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\address_relations\output-000003.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\address_relations\output-000004.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\address_relations\output-000005.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\address_relations\output-000006.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\address_relations\output-000007.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\address_relations\output-000008.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\address_relations\output-000009.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\address_relations\output-000010.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\address_relations\output-000011.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\address_relations\output-000012.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\address_relations\output-000013.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\address_relations\output-000014.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\address_relations\output-000015.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\address_relations\output-000016.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\address_relations\output-000017.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\address_relations\output-000018.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\address_relations\output-000019.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\address_relations\output-000020.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\address_relations\output-000021.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\address_relations\output-000022.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\address_relations\output-000023.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\address_relations\output-000024.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\address_relations\output-000025.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\address_relations\output-000026.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\address_relations\output-000027.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\address_relations\output-000028.csv
"
```

### Database Structure
#### Nodes

| Property               | Description                                       |
|:-----------------------|:--------------------------------------------------|
| address_id:ID(Address) | internal address id                               |
| address                | address                                           |
| first_tx_id:           | first transaction id related to this address      |
| last_tx_id:            | last transaction id related to this address       |
| no_incoming_txs:int    | number of incoming transactions to this address   |
| no_outgoing_txs:int    | number of outgoing transactions from this address |
| in_degree:             |                                                   |
| out_degree:            |                                                   |
| is_fraud_address       | True if the address is a fraudulent entity        |
| is_defi_protocol       | True if the address is a defi protocol            |
| fa_category            | Category of fraud                                 |
| fa_reporter            | Reporter of fraud                                 |
| fa_url                 | URL linked to this fraud                          |
| fa_description         | Description of the fraud                          |
| fa_origin              | Origin File of the fraud                          |
| p_label                | Protocol Label                                    |
| p_type                 | Protocol Type                                     |
| p_protocol             | Protocol                                          |

#### Relationships:

| Property            | Description                                                        |
|:--------------------|:-------------------------------------------------------------------|
| :START_ID(Address)  | internal id from which the relation originates                     |
| :END_ID(Address)    | internal id to which the relation points to                        |
| value:float         | value of all aggregated transactions between the related Addresses |
| no_transactions:int | number of all transactions between the related Addresses           |





## Network Study

Inside of [data/network/output/network_study_outputs](data/network/output/network_study_outputs)
are various files, that were extracted from the Network data and were used for the network study.
- [address_category.csv](data/network/output/network_study_outputs/address_category.csv)
   Lists all fraudulent addresses with their respective category.
- [fraudulent_addresses_in_out_degree.csv](data/network/output/network_study_outputs/fraudulent_addresses_in_out_degree.csv)
  Lists all ingoing and outgoing degrees for fraudulent addresses + their category
- [defi_addresses_in_out_degree.csv](data/network/output/network_study_outputs/defi_addresses_in_out_degree.csv)
  Lists all ingoing and outgoing degrees for DeFi protocols + protocol type and protocol
- [fraudulent_addresses_amount_of_neighbours.csv](data/network/output/network_study_outputs/fraudulent_addresses_amount_of_neighbours.csv)
  Lists the amount of neighbours for all fraudulent addresses
- [fraud_neighbour_percentage_including_addresses_with_no_neighbours](data/network/output/network_study_outputs/fraud_neighbour_percentage_including_addresses_with_no_neighbours.xlsx)
  Lists the number of fraud addresses in their category + the average clustering coefficient per category
- [clustering_coefficient.csv](data/network/output/network_study_outputs/clustering_coefficient.csv)
  Lists all clustering coefficients for fraudulent addresses. NOTE: some of the coefficients are > 1 because the network is bi directed, 
  and the coefficients in this file were calculated like the network was undirected. Therefor this file is not used.
- [clustering_coefficient_0_1.csv](data/network/output/network_study_outputs/clustering_coefficient_0_1.csv)
  Same file as [clustering_coefficient.csv](data/network/output/network_study_outputs/clustering_coefficient.csv) but without nan values
- [clustering_coefficient_0_1.xlsx](data/network/output/network_study_outputs/clustering_coefficient_0_1.xlsx)
  File with clustering coefficients aswell as in and out degrees graphs for fraud addresses and DeFi addresses 
  Sheets:
  - <b>clust_coeff_directed_correct</b>: contains the correct clustering coefficients for the directed network. Basically all the wrongly calculated CC`s can be devided by 2
  - <b>clust_coeff_undirected_wrong</b>: contains the clustereng coefficients if the network was undirected  -> there are entries > 0 (which is wrong)
  - <b>in and out degrees fraud</b>: contains the in and out degrees for fraud addresses and their category
  - <b>in and out degrees defi</b>: contains the in and out degrees for DeFi addresses, their protocol types and the protocols

## Transaction Analysis
In the file [queries_output.xlsx](queries_output.xlsx) are various Sheets, that were used to gain information about how DeFi and Frauduelent addresses 
are connected, as well as how much value flows from fraudulent addresses to DeFi protocols:
 - <b>(d)-(#1..3)-(f) ~ any</b>: Shows the sum of values flowing from fradulent addresses to DeFi protocols in heterogenous paths up to 3 hops.
   This Sheet is a summary of the next 3 sheets.
 - <b>1 HOP any</b>: Shows the sum of values flowing from fradulent addresses to DeFi protocols in 1 hop(s).
 - <b>2 HOP any</b>: Shows the sum of values flowing from fradulent addresses to DeFi protocols in 2 hop(s). 
 As the branches get pretty wild after 1 hop, we decided not to use this data.
 - <b>3 HOP any</b>: Shows the sum of values flowing from fradulent addresses to DeFi protocols in 3 hop(s). 
 As the branches get pretty wild after 1 hop, we decided not to use this data.
 - <b>(d)-(f1..3)-(f)  ~ fraud path</b>: Shows the sum of values flowing from fradulent addresses to DeFi protocols in homogenous (fraudulent) paths up to 3 hops.
   This Sheet is a summary of the next 3 sheets.
 - <b>1 HOP fraud path</b>: Shows the sum of values flowing from fradulent addresses to DeFi protocols in 1 hop(s).
 - <b>2 HOP fraud path</b>: Shows the sum of values flowing from fradulent addresses to DeFi protocols in 2 hop(s), where all in-between connections are also fraudulent. 
 As the branches get pretty wild after 1 hop, we decided not to use this data.
 - <b>3 HOP fraud path</b>: Shows the sum of values flowing from fradulent addresses to DeFi protocols in 3 hop(s), where all in-between connections are also fraudulent. 
 As the branches get pretty wild after 1 hop, we decided not to use this data.
 - <b>1 HOP GROUPED BY FRAUD</b>: Shows the transaction value from fraudulent addresses to DeFi protocols by fraud category, protocols and protocol type.
   In Columns K - U we see a Table that shows the sum of transaction value per fraud category and per protocol type and protocol.  
   The data is then shown in the graphs in Column W - AC
 - <b>Addresses Connected to Uniswap</b>: In this Sheet we see all the fraudulent addresses and their categories, 
   that are connected to uniswap + their respective transaction values.
 - <b>fraud_neighbours_pctg_0_include</b>:
   In this sheet we see which fraudulent addresses have how many fraudulent neighbours. 
   Columns A-F are the normal List view, and Columns J to W show a transposed view which is used to 
   show the graphs to the right of Column Y. This sheet includes all percentages.
 - <b>fraud neighbours_pctg_0_exclude</b>: Shows the same data as fraud_neighbours_pctg_0_include, but entries that do not have any fraudulent neighbours are removed.
 - <b>fraud_neighbours_percentage_grp</b>: Shows how many fraudulent neighbours occured in which fraud category
 - <b>addresses connected to defi</b>: Shows the amount of fraudulent addresses directly connected to Defi protocols.

### Trace Analysis
In the file [trace_analysis.xlsx](trace_analysis.xlsx) 
and [trace_analysis_without_error_traces.xlsx](trace_analysis_without_error_traces.xlsx)
are Sheets related to message traces and their evaluation:
- <b>traces_with_signatures</b>: Shows all message traces connected to the uniswap protocol, with the respective text_signatures of the function calls.
- <b>trace lenghts</b>: Shows the lengths of traces, and how often the trace lengths occur.
- <b>traces_sorted_by_i_tx_val</b>: Only for the trace_analysis_without_error_traces - shows the traces sorted by their initial transaction value.
- <b>trace_len_gb_initial_tx_value</b>: Shows the correlation between different trace lenghts and the initial transaction value that was sent in the traces.
- <b>patterns</b>: Shows the top 10 trace patterns by occurance and the top 10 patterns by associated initial transaction value
- <b>top10patterns_by_occurance</b>: Shows the top 10 most occuring patterns in more detail. Text signatures marked in green are the assumed function calls.
- <b>top10patterns_by_value</b>: Shows the top 10 patterns with the highest associated intial value
- <b>to_addrs_of_initial_traces</b>: Shows the Addresses of the Uniswap Protocol, and how much value was transferred to which address.
- <b>erc20address_occurances</b>: erc20address_occurances shows all the tokens occuring the most often in message traces.  
  Column D describes the transaction value, that directly went to the erc20 address, 
  Column E describes the sum of initial tx values where the address was involved in a trace
  Token names were extracted from Etherscan and are shown for the top 25 occuring tokens, aswell as the top 25 associated values

All of the above shown data is obtained by running the scripts inside of [data\traces\converter.py](/data/traces/converter.py)


Inside `data/traces/converter.py` we provide the following commands:
```
merge-trace-files
get-unknown-addresses
enrich-addresses
handle-nulls
get-malicious-traces-connected-to-uniswap
do-trace-analysis [--skip-traces-with-errors]
get-all-signatures
```

Usage:  `converter.py [OPTIONS] COMMAND [ARGS]...`
e.g.: `python converter.py merge-trace-files`

By running 
```
merge-trace-files
get-unknown-addresses
enrich-addresses
handle-nulls
```
in this order, we are set up to again import a network to Neo4j with the following command 
(AGAIN MUST BE ONE LINE ONLY + REPLACE PATH WITH CORRECT FILE LOCATIONS!):

    neo4j-admin import --database traces --nodes=Address="C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\traces\output\addresses\enriched\header.csv,C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\traces\output\addresses\enriched\all_with_null.csv" --relationships=TRANSACTIONS="C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\traces\output\malicious_traces\header.csv,C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\traces\output\malicious_traces\malicious_traces_checked.csv"


Readable command:
```
neo4j-admin import --database traces --nodes=Address="
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\traces\output\addresses\enriched\header.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\traces\output\addresses\enriched\all_with_null.csv
" --relationships=TRANSACTIONS="
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\traces\output\malicious_traces\header.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\traces\output\malicious_traces\malicious_traces_checked.csv
"
```

We then again follow the steps in [Steps to import data with neo4j admin](#steps-to-import-data-with-neo4j-admin).
Only step 7 is different where instead of `create database defi` we now use another database name therefor `create database traces`.

The 3 commands
```
get-malicious-traces-connected-to-uniswap
do-trace-analysis [--skip-traces-with-errors]
get-all-signatures
```

cover the trace analysis and provide us with the data presented in
[trace_analysis.xlsx](trace_analysis.xlsx) and [trace_analysis_without_error_traces.xlsx](trace_analysis_without_error_traces.xlsx) covered above.