import os
import time
import typer
import pandas as pd
import numpy as np
import re
import base64
import time
import json
import requests
from retrying import retry

app = typer.Typer()

# for all trace file
@app.command() # mandatory
def merge_trace_files():
    filepath = os.path.join(os.path.dirname(__file__), "..", "..", "data/traces/input/malicious_traces/decompressed")

    total_time = time.time()

    result_df = pd.DataFrame()

    for filename in os.listdir(filepath):
        t0 = time.time()
        if "traces_fraud_" in filename:
            print(f"Processing {filename}...")
            f = os.path.join(filepath, filename)
            # checking if it is a file
            if os.path.isfile(f):
                df = pd.read_csv(f)
                df['value'] = df['value'].astype(np.float64) / 1.0e18 #convert wei to ETH
                result_df = pd.concat([result_df, df], ignore_index=True, axis=0)

            print("Processed in", time.time() - t0, "seconds")
    result_df.to_csv("output/malicious_traces/malicious_traces_merged.csv", header=False, index=False)
    print("Total time spent:", time.time() - total_time, "seconds")

# checks inside malicous_traces_merged, if there are addresses, that are not in uniswap_addresses.csv or fraud_addresses_connected_to_uniswap.scv
# addresses that are not included in either are supposed to be from UNI2 probably or other unknown uni addresses not in our dataset
@app.command() #mandatory
def get_unknown_addresses():
    traces = pd.read_csv("output/malicious_traces/malicious_traces_merged.csv")
    fa_connected_to_uniswap = pd.read_csv("input/fraud_addresses_connected_to_uniswap.csv")
    uniswap_addresses = pd.read_csv("input/uniswap_addresses.csv")

    print(len(traces), "rows in malicious_traces_merged.csv")
    from_unknown = pd.DataFrame()
    to_unknown = pd.DataFrame()

    t0 = time.time()

    from_unknown = traces[
        ~traces["from_address"].isin(fa_connected_to_uniswap["address"])
        &
        ~traces["from_address"].isin(uniswap_addresses["address"])
        ]["from_address"]

    to_unknown = traces[
        ~traces["to_address"].isin(fa_connected_to_uniswap["address"])
        &
        ~traces["to_address"].isin(uniswap_addresses["address"])
        ]["to_address"]

    from_unknown.head()
    to_unknown.head()

    unknown_df = pd.concat([from_unknown, to_unknown], ignore_index=True, axis=0).drop_duplicates()

    print(len(unknown_df), "addresses unknown")
    print("Processed in", time.time() - t0, "seconds")
    unknown_df.to_csv("output/addresses/unknown_addresses.csv", header=["address"], index=False)
    fa_connected_to_uniswap.to_csv("output/addresses/fraud_addresses_connected_to_uniswap.csv", header=["address"], index=False)
    uniswap_addresses.to_csv("output/addresses/uniswap_addresses.csv", header=["address"], index=False)
    print("Total time spent:", time.time() - t0, "seconds")

# enriches the 3 address files with the properties from the full network
@app.command() #mandatory
def enrich_addresses():
    columns = [
        "address_id",
        "address",
        "first_tx_id",
        "last_tx_id",
        "no_incoming_txs",
        "no_outgoing_txs",
        "in_degree",
        "out_degree",
        "is_fraud_address",
        "is_defi_protocol",
        "fa_category",
        "fa_reporter",
        "fa_url",
        "fa_description",
        "fa_origin",
        "p_label",
        "p_type",
        "p_protocol"
    ]
    types = {
        #"address_id" : np.int64,
        "address" : str,
        "first_tx_id" : 'Int64',
        "last_tx_id" : 'Int64',
        "no_incoming_txs" : 'Int64',
        "no_outgoing_txs" : 'Int64',
        "in_degree" : 'Int64',
        "out_degree" : 'Int64',
        "is_fraud_address" : bool,
        "is_defi_protocol" : bool,
        "fa_category" : str,
        "fa_reporter": str,
        "fa_url" : str,
        "fa_description" : str,
        "fa_origin" : str,
        "p_label" : str,
        "p_type" : str,
        "p_protocol" : str
    }



    fa_connected_to_uniswap = pd.read_csv("output/addresses/fraud_addresses_connected_to_uniswap.csv",usecols=["address"])
    uniswap_addresses = pd.read_csv("output/addresses/uniswap_addresses.csv", usecols=["address"])
    unknown_addresses = pd.read_csv("output/addresses/unknown_addresses.csv", usecols=["address"])

    # add empty properties to all dataframes
    # but do not overwrite the address column as it already exists
    for col in columns[2::]:
        fa_connected_to_uniswap[col] = None
        uniswap_addresses[col] = None
        unknown_addresses[col] = None


    filepath = os.path.join(os.path.dirname(__file__), "..", "..", "data/network/output/addresses")
    t0 = time.time()
    for filename in os.listdir(filepath):
        t0 = time.time()
        if "output" in filename:
            print(f"Processing {filename}...")
            f = os.path.join(filepath, filename)
            # checking if it is a file
            if os.path.isfile(f):
                network_df = pd.read_csv(f,names=columns)


                # replace all rows inside of the files with the enriched version row from the network/output/addresses folder
                matches_df = fa_connected_to_uniswap[["address"]].merge(network_df.drop_duplicates("address", keep="last"), on = "address", how = "left")
                fa_connected_to_uniswap.update(matches_df[columns])
                matches_df = uniswap_addresses[["address"]].merge(network_df.drop_duplicates("address", keep="last"), on = "address", how = "left")
                uniswap_addresses.update(matches_df[columns])
                matches_df = unknown_addresses[["address"]].merge(network_df.drop_duplicates("address", keep="last"), on="address", how="left")
                unknown_addresses.update(matches_df[columns])

                print("Processed in", time.time() - t0, "seconds")

    # converting int columns to int types
    fa_connected_to_uniswap = fa_connected_to_uniswap.astype(types)
    uniswap_addresses = uniswap_addresses.astype(types)
    unknown_addresses = unknown_addresses.astype(types)
    # put all addresses in one file and remove duplicates
    all_df = pd.concat([fa_connected_to_uniswap, uniswap_addresses, unknown_addresses], ignore_index=True, axis=0).drop_duplicates("address", keep="last")

    fa_connected_to_uniswap.to_csv("output/addresses/enriched/fraud_addresses_connected_to_uniswap.csv", header=False, index=False)
    uniswap_addresses.to_csv("output/addresses/enriched/uniswap_addresses.csv", header=False, index=False)
    unknown_addresses.to_csv("output/addresses/enriched/unknown_addresses.csv", header=False, index=False)
    all_df.to_csv("output/addresses/enriched/all.csv", header=False, index=False)

    print("Total time spent:", time.time() - t0, "seconds")

@app.command() #mandatory
def handle_nulls():
    # where from_address or to_address in the traces are NaN replace the data with the string "null" for Neo4J
    cols = [
    "Unnamed",
    "transaction_index",
    "from_address",
    "to_address",
    "value",
    "input",
    "output",
    "trace_type",
    "call_type",
    "reward_type",
    "gas",
    "gas_used",
    "subtraces",
    "trace_address",
    "error",
    "status",
    "trace_id",
    "transaction_hash",
    "block_number",
    "block_id_group"
    ]

    traces_df = pd.read_csv("output/malicious_traces/malicious_traces_merged.csv", names=cols)
    traces_df.loc[traces_df["from_address"].isna(), "from_address"] = "null"
    traces_df.loc[traces_df["to_address"].isna(), "to_address"] = "null"
    traces_df.to_csv("output/malicious_traces/malicious_traces_checked.csv", header=False, index=False)

    # add an address with address "null" to the addresses file
    cols = [
        "address",
        "first_tx_id",
        "last_tx_id",
        "no_incoming_txs",
        "no_outgoing_txs",
        "in_degree",
        "out_degree",
        "is_fraud_address",
        "is_defi_protocol",
        "fa_category",
        "fa_reporter",
        "fa_url",
        "fa_description",
        "fa_origin",
        "p_label",
        "p_type",
        "p_protocol"
    ]
    all_df = pd.read_csv("output/addresses/enriched/all.csv",names=cols)

    null_df = pd.DataFrame({
        "address": ["null"],
        "first_tx_id": [0],
        "last_tx_id": [0],
        "no_incoming_txs": [0],
        "no_outgoing_txs": [0],
        "in_degree": [0],
        "out_degree": [0],
        "is_fraud_address": [False],
        "is_defi_protocol": [False],
        "fa_category": [""],
        "fa_reporter": [""],
        "fa_url": [""],
        "fa_description": [""],
        "fa_origin": [""],
        "p_label": [""],
        "p_type": [""],
        "p_protocol": [""]
    })

    types = {
        # "address_id": np.int64,
        "address": str,
        "first_tx_id": 'Int64',
        "last_tx_id": 'Int64',
        "no_incoming_txs": 'Int64',
        "no_outgoing_txs": 'Int64',
        "in_degree": 'Int64',
        "out_degree": 'Int64',
        "is_fraud_address": bool,
        "is_defi_protocol": bool,
        "fa_category": str,
        "fa_reporter": str,
        "fa_url": str,
        "fa_description" : str,
        "fa_origin": str,
        "p_label": str,
        "p_type": str,
        "p_protocol": str
    }

    all_df = pd.concat([all_df, null_df], ignore_index=True, axis=0)
    all_df = all_df.astype(types)
    all_df.to_csv("output/addresses/enriched/all_with_null.csv", header=False, index=False)


@app.command() #mandatory
def get_malicious_traces_connected_to_uniswap():
    cols = [
        "Unnamed",
        "transaction_index",
        "from_address",
        "to_address",
        "value",
        "input",
        "output",
        "trace_type",
        "call_type",
        "reward_type",
        "gas",
        "gas_used",
        "subtraces",
        "trace_address",
        "error",
        "status",
        "trace_id",
        "transaction_hash",
        "block_number",
        "block_id_group"
    ]

    traces_df = pd.read_csv("output/malicious_traces/malicious_traces_merged.csv", names=cols)
    uniswap_addresses = pd.read_csv("input/uniswap_addresses.csv")["address"]
    initial_traces = traces_df[traces_df["trace_address"].isna()]
    print(len(initial_traces))

    initial_traces_to_uniswap = initial_traces[initial_traces["to_address"].isin(uniswap_addresses.values)]

    traces_connected_to_uniswap = traces_df[traces_df["transaction_hash"].isin(initial_traces_to_uniswap["transaction_hash"].values)]
    print(len(traces_connected_to_uniswap))

    traces_connected_to_uniswap.to_csv("output/malicious_traces/malicious_traces_connected_to_uniswap.csv", header=False, index=None)

    #remove input and output columns, as Excel cannot handle the data and overflows Cells
    traces_connected_to_uniswap = traces_connected_to_uniswap.drop("input", 1)
    traces_connected_to_uniswap = traces_connected_to_uniswap.drop("output", 1)
    traces_connected_to_uniswap.to_csv("output/malicious_traces/malicious_traces_connected_to_uniswap_xlsx_safe.csv", header=False, index=None)

@app.command()
def do_trace_analysis(skip_traces_with_errors: bool = False):
    cols = [
        "Unnamed",
        "transaction_index",
        "from_address",
        "to_address",
        "value",
        "input",
        "output",
        "trace_type",
        "call_type",
        "reward_type",
        "gas",
        "gas_used",
        "subtraces",
        "trace_address",
        "error",
        "status",
        "trace_id",
        "transaction_hash",
        "block_number",
        "block_id_group"
    ]
    if skip_traces_with_errors:
        out_dir = "output/trace_analysis_results/error_traces_excluded/"
    else:
        out_dir = "output/trace_analysis_results/error_traces_included/"

    quit()

    all_traces_df = pd.read_csv("output/malicious_traces/malicious_traces_connected_to_uniswap.csv", names=cols)

    # group traces by transaction_hash and check similaritites
    tx_hashes = all_traces_df["transaction_hash"]
    unique_tx_hashes = tx_hashes.unique()

    print(len(tx_hashes))  # 76568
    print(len(unique_tx_hashes))  # 8035

    traces_grouped_by_txs = pd.DataFrame(columns=["transaction_hash", "full_trace", "trace_length", "initial_to_address", "initial_value"])
    idx = 1
    overall_traces = 0
    skipped_tx_hashes = []

    for tx_hash in unique_tx_hashes:
        print(f"CHECK {tx_hash}, {idx}/{len(unique_tx_hashes)}")
        idx += 1
        if idx == 1:  # set > 1 for testing only a certain range
            break
        # get all traces with specific tx_hash
        trace_df = all_traces_df[all_traces_df['transaction_hash'] == tx_hash]
        overall_traces += len(trace_df)

        if skip_traces_with_errors and 0 in trace_df["status"].values:
            skipped_tx_hashes.append(tx_hash)
            continue

        # get full trace, exclude irrelevant columns and replace the initial from_address with "IRRELEVANT"
        # to construct a String of the trace that can be compared with other traces
        full_trace_original = trace_df.filter(["from_address", "to_address", "trace_address", "value"])
        full_trace = trace_df.filter(["from_address", "to_address", "trace_address", "value"])
        full_trace.loc[full_trace["trace_address"].isna(), "from_address"] = "IRRELEVANT"
        full_trace_as_string = ""
        for index, row in full_trace.iterrows():
            full_trace_as_string += str(row["from_address"])+"_"+str(row["to_address"])+"_"+str(row["trace_address"])+"_"

        # get initial to_address
        # get trace length
        trace_length = len(full_trace_original)
        # get initial to_address
        initial_to_address = trace_df.iloc[0]["to_address"]
        # get initial value
        initial_value = trace_df[trace_df["trace_address"].isna()].iloc[0]["value"]
        # pack everything into one row and save it in traces_grouped_by_txs
        trace = pd.DataFrame(
            [[tx_hash, full_trace_original, trace_length, initial_to_address, initial_value, full_trace_as_string]],
            columns=["transaction_hash", "full_trace", "trace_length", "initial_to_address", "initial_value", "full_trace_as_string"]
        )

        traces_grouped_by_txs = pd.concat([traces_grouped_by_txs, trace], ignore_index=True, axis=0)

    print(f"Skipped {len(skipped_tx_hashes)} faulty transactions...")

    # TRACE_LENGTH : AMOUNT
    traces_length_counts = traces_grouped_by_txs['trace_length'].value_counts()
    traces_length_counts.to_csv(out_dir+"traces_length_counts.csv")

    # TRACE_LENGTH : INITIAL_VALUE_GROUP : AMOUNT
    traces_sorted_by_initial_tx_value = traces_grouped_by_txs.sort_values(by="initial_value")
    traces_sorted_by_initial_tx_value[["transaction_hash", "trace_length", "initial_to_address","initial_value"]].to_csv(out_dir+"traces_sorted_by_initial_tx_value.csv")

    bins = [-0.1, 0, 0.1, 1, 10, 100, 1000, 10000]
    amount_traces_by_initial_tx_value = traces_grouped_by_txs["initial_value"].value_counts(bins=bins)
    amount_traces_by_initial_tx_value.to_csv(out_dir+"amount_traces_by_initial_tx_value.csv")


    # INITIAL_TO_ADDRESS : AMOUNT
    initial_to_address_counts = traces_grouped_by_txs['initial_to_address'].value_counts()
    initial_to_address_counts.to_csv(out_dir+"initial_to_address_counts.csv")

    # TX_HASH IDENTIFYING THE PATTERN : AMOUNT
    full_string_dict = {}
    group_count_dict = {}
    value_sum_dict = {}
    from_dict = {}
    for index, row in traces_grouped_by_txs.iterrows():

        initial_from_address = row["full_trace"]["from_address"].iloc[0]
        if row["full_trace_as_string"] in full_string_dict:
            # trace pattern already found -> count up 1 for the existing identifying transaction hash
            tx_hash_identifying_pattern = full_string_dict[row["full_trace_as_string"]]
            group_count_dict[tx_hash_identifying_pattern] += 1
            value_sum_dict[tx_hash_identifying_pattern] += np.float64(row["initial_value"])

            #if initial from address did not appear in transactions with this pattern, add
            if(initial_from_address not in from_dict[tx_hash_identifying_pattern]):
                from_dict[tx_hash_identifying_pattern].append(initial_from_address)

        else:
            # new pattern found -> save the transaction hash as identifier for the newly found trace pattern in group_count_dict
            tx_hash_identifying_pattern = row["transaction_hash"]
            full_string_dict[row["full_trace_as_string"]] = tx_hash_identifying_pattern
            group_count_dict[tx_hash_identifying_pattern] = 1
            value_sum_dict[tx_hash_identifying_pattern] = np.float64(row["initial_value"])

            from_dict[tx_hash_identifying_pattern] = [initial_from_address]


    pattern_df = pd.DataFrame(group_count_dict.items(),columns=["tx_hash_identifying_the_pattern", "pattern_occurrance"])
    pattern_df = pattern_df.sort_values(by=['pattern_occurrance'], ascending=False)

    value_sum_df = pd.DataFrame(value_sum_dict.items(), columns=["tx_hash_identifying_the_pattern", "sum"])
    pattern_df = pattern_df.merge(value_sum_df, left_on="tx_hash_identifying_the_pattern", right_on="tx_hash_identifying_the_pattern", how="left")


    for key in from_dict.keys():
        from_dict[key] = len(from_dict[key])
    from_df = pd.DataFrame(from_dict.items(), columns=["tx_hash_identifying_the_pattern", "amount_of_different_from_addresses"])
    pattern_df = pattern_df.merge(from_df, left_on="tx_hash_identifying_the_pattern", right_on="tx_hash_identifying_the_pattern", how="left")

    pattern_df.to_csv(out_dir+"pattern_counts.csv", header=True, index=False)

    # ER20Addresses occuring in traces:
    erc20addresses_occurance_dict = {}
    erc20addresses_initial_value_dict = {}

    erc20df = pd.DataFrame(columns = ['to_address', 'occurance', 'value'])
    erc20addresses = pd.read_csv("input/ERC20_addresses/addr_ERC20_until20211231.csv")
    for index, row in traces_grouped_by_txs.iterrows():
        handled = {}
        for i, r in row["full_trace"].iterrows():
            if r["to_address"] in erc20addresses["address"].values:
                # if to_address already in erc20 dict -> update values
                if r["to_address"] in erc20addresses_occurance_dict:

                    match_address = (erc20df["to_address"] == r["to_address"])
                    erc20df.loc[match_address, 'occurance'] = erc20df.loc[match_address]["occurance"] + 1
                    erc20df.loc[match_address, 'value'] = erc20df.loc[match_address]["value"] + r["value"]
                    erc20addresses_occurance_dict[r["to_address"]] += 1

                    # add the initial value of a transaction 1 time to the erc20 address if the address occurs inside of the trace
                    if r["to_address"] not in handled:
                        erc20addresses_initial_value_dict[r["to_address"]] += np.float64(row["initial_value"])
                        handled[r["to_address"]] = True
                # if to_address not in erc20 dict -> add
                else:
                    current = pd.DataFrame([[r["to_address"], 1, r["value"]]],
                                           columns=['to_address', 'occurance', 'value'])
                    erc20df = pd.concat([erc20df, current])
                    erc20addresses_occurance_dict[r["to_address"]] = 1

                    # add the initial value of a transaction 1 time to the erc20 address if the address occurs inside of the trace
                    if r["to_address"] not in handled:
                        erc20addresses_initial_value_dict[r["to_address"]] = np.float64(row["initial_value"])
                        handled[r["to_address"]] = True

    erc20addresses_initial_value_df = pd.DataFrame(erc20addresses_initial_value_dict.items(), columns=["to_address", "sum_of_initial_values"])
    erc20df = erc20df.merge(erc20addresses_initial_value_df, left_on="to_address", right_on="to_address", how="left")

    erc20df = erc20df.sort_values(by="occurance", ascending=False)
    erc20df.to_csv(out_dir+"erc20addresses_occurance_df.csv", mode="w", header="True", index=None)


@app.command() # retreived on 29.05.22 13:00
def get_all_signatures():
    result = requests.get("https://www.4byte.directory/api/v1/signatures/")
    signatures = result.json().get("results")
    total = result.json().get("count")  # 808410 signatures
    pages = int(np.ceil(total/100))
    total_df = pd.DataFrame()
    for page in range(1, pages+1):  # 100 signatures per page
        page_results = requests.get("https://www.4byte.directory/api/v1/signatures/?page="+str(page))
        items = page_results.json().get("results")
        df = pd.json_normalize(items)
        df.columns = ["id", "created_at", "text_signature", "hex_signature", "bytes_signature"]
        df = df.filter(["id", "created_at", "text_signature", "hex_signature"])
        total_df = pd.concat([total_df, df], ignore_index=True, axis=0)
        print(f"Retreived page {page}/{pages}: {len(total_df)} items found.")
    total_df.to_csv("input/signatures.csv", mode="w", header="True", index=None)



@app.command()
def merge_traces_with_signatures():
    cols = [
        "Unnamed",
        "transaction_index",
        "from_address",
        "to_address",
        "value",
        "input",
        "output",
        "trace_type",
        "call_type",
        "reward_type",
        "gas",
        "gas_used",
        "subtraces",
        "trace_address",
        "error",
        "status",
        "trace_id",
        "transaction_hash",
        "block_number",
        "block_id_group"
    ]
    pd.set_option("display.max_rows", None)

    all_traces_df = pd.read_csv("output/malicious_traces/malicious_traces_connected_to_uniswap.csv", names=cols)
    #all_traces_df.sort_values(by=["transaction_hash", "transaction_index"], inplace=True) # is already sorted as is

    all_traces_df["hex_signature"] = all_traces_df["input"].str[:10]
    all_traces_df["hex_signature"] = all_traces_df["hex_signature"].astype(str)

    signatures_df = pd.read_csv("input/signatures.csv", header=0, names=["id", "created_at", "text_signature", "hex_signature"])
    signatures_df["hex_signature"] = signatures_df["hex_signature"].astype(str)

    merged_df = all_traces_df.merge(signatures_df, left_on="hex_signature", right_on="hex_signature", how="left")
    merged_df.sort_values(by=["transaction_hash", "transaction_index"], inplace=True)

    merged_df.to_csv("output/trace_analysis_results/traces_with_signatures.csv", mode="w", header="True", index=None)
    merged_df = merged_df.drop("input", 1)
    merged_df = merged_df.drop("output", 1)
    merged_df.to_csv("output/trace_analysis_results/traces_with_signatures_xlsx_safe.csv", mode="w", header="True", index=None)



def retry_if_connection_error(exception):
    """ Specify an exception you need. or just True"""
    #return True
    return isinstance(exception, ConnectionError)

@retry(retry_on_exception=retry_if_connection_error, wait_fixed=2000)
def get_function_signature(signature, **kwargs):
     #signature e.g.: "0xabcd1234"
    return requests.get("https://www.4byte.directory/api/v1/signatures/?hex_signature=" + signature)



@app.command()
def enrich_found_erc20_addresses():
    df = pd.read_csv("output/trace_analysis_results/pattern_counts.csv")


@app.command()
def exit():
    pass

def main():
    app()

if __name__ == "__main__":
    main()
