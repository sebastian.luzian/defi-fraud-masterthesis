import os
import time
import typer
import pandas as pd
import numpy as np
import re
import base64
import time

app = typer.Typer()

@app.command()
def prepare_defi_and_fraud_addresses():
    defi = pd.read_csv("input/addresses/db_defi_addresses_export.csv")
    fraud = pd.read_csv("input/addresses/db_fraud_addresses_export.csv")

    # remove_addresses from defi if they are also labeled fraud (rare but possible)
    defi = defi[defi["a.is_fraud_address"] == False]
    defi.to_csv("output/addresses/defi_addresses.csv", mode="w", header=False, index=None)


# for addresses and address_relations
@app.command() # mandatory - concat neighbours files into one file
def prepare_neighbour_addresses():
    # neighbours1 and neighbours2 into one file
    neighbours1 = pd.read_csv("input/addresses/neighbours1_export.csv")
    neighbours2 = pd.read_csv("input/addresses/neighbours2_export.csv")
    neighbours2 = neighbours2.drop(['id(n)','n.address'], axis=1)
    all_neighbours = neighbours1.join(neighbours2)

    # uniqueify
    columns = [
        "id",
        "address",
        "first_tx_id",
        "last_tx_id",
        "no_incoming_txs",
        "no_outgoing_txs",
        "in_degree",
        "out_degree",
        "is_fraud_address",
        "is_defi_protocol",
        "fa_category",
        "fa_reporter",
        "fa_url",
        "fa_description",
        "fa_origin",
        "p_label",
        "p_type",
        "p_protocol"
    ]
    all_neighbours.columns = columns
    key_dict = {}
    for index, row in all_neighbours.iterrows():
        #overwrites less reliable entry with more reliable entry
        key_dict[row["id"]] = row

    print(len(key_dict), "unique neighbour addresses found.")
    neighbours_df = pd.DataFrame.from_dict(key_dict, orient="index", columns=columns)

    # filter all fraud and defi addresses - those are provided in seperate files already
    neighbours_df = neighbours_df[neighbours_df["is_fraud_address"] == False]
    neighbours_df = neighbours_df[neighbours_df["is_defi_protocol"] == False]
    neighbours_df = neighbours_df[neighbours_df["is_defi_protocol"] == True and neighbours_df["is_fraud_address"] == True]

    print(len(neighbours_df), "addresses total.")

    neighbours_df.to_csv("output/addresses/neighbours.csv", mode="w", header=False, index=None)




@app.command() # mandatory - remove headers from fraud, defi and transactions files
def remove_headers():
    txs = pd.read_csv("input/address_relations/db_txs_from_fraud_addresses_export.csv")
    fa = pd.read_csv("input/addresses/db_fraud_addresses_export.csv")
    da = pd.read_csv("input/addresses/db_defi_addresses_export.csv")

    txs.to_csv("output/address_relations/transactions.csv", mode="w", header=False, index=None)
    fa.to_csv("output/addresses/fraud_addresses.csv", mode="w", header=False, index=None)
    da.to_csv("output/addresses/defi_addresses.csv", mode="w", header=False, index=None)

def main():
    app()

if __name__ == "__main__":
    main()
