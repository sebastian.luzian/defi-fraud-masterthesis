## One-hop-network

The one-hop-network is completely optional, as it is a 
subset of the original network, and was anly used to try speed up some of the calculationswe did.

The one-hop network can be imported to Neo4j via the neo4j-admin tool
with the following command:

    neo4j-admin import --database onehop --nodes=Address="
    C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\one_hop_network\output\addresses\header.csv,
    C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\one_hop_network\output\addresses\neighbours.csv,
    C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\one_hop_network\output\addresses\defi_addresses.csv,
    C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\one_hop_network\output\addresses\fraud_addresses.csv
    " --relationships=TRANSACTIONS="
    C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\one_hop_network\output\address_relations\header.csv,
    C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\one_hop_network\output\address_relations\transactions.csv
    "

as a ONE-LINER:

    neo4j-admin import --database onehop --nodes=Address="    C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\one_hop_network\output\addresses\header.csv,    C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\one_hop_network\output\addresses\neighbours.csv,    C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\one_hop_network\output\addresses\defi_addresses.csv,    C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\one_hop_network\output\addresses\fraud_addresses.csv    " --relationships=TRANSACTIONS="    C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\one_hop_network\output\address_relations\header.csv,    C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\one_hop_network\output\address_relations\transactions.csv    "

Make sure to change the paths to the corresponding files. 

We then follow the general approch for all the Neo4j Impots:

1) Create a new Project 
2) Add DBMS - Local DBMS with password "password"
3) Database we want to create must not exist yet
4) Stop the DBMS
5) Run the command from below in the Neo4J Admin Tool location (in one line - https://lingojam.com/TexttoOneLine)
6) Start the DBMS
7) Inside Neo4J Browser run ":use system" and "create database defi"
8) Stop the DBMS
9) Start the DBMS

Steps 1-4 can be skipped if a project is already created, and if we want to import the data in an already existing project.