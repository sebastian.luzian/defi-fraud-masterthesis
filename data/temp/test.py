import typer
import pandas as pd

app = typer.Typer()

# Returns all the fraud addresses, that are in unique_addresses.csv but NOT in found_fraud_addresses_after_import.csv
# Prints all the addresses that were not included in the network data.
# The addresses were not included in the network data, as they don`t have any transactions
@app.command()
def not_contained():
    df1 = pd.read_csv("../malicious_addresses/output/unique_addresses.csv")
    df2 = pd.read_csv("input/found_fraud_addresses_after_import.csv")

    key_dict = {}
    for index, row in df1.iterrows():
        #overwrites less reliable entry with more reliable entry
        key_dict[row['address']] = 1
    for index, row in df2.iterrows():
        key_dict[row['fa.address']] = key_dict[row['fa.address']] +1

    non_duplicates_dict = dict()
    # Iterate over all the items in dictionary and filter items which has even keys
    for (key, value) in key_dict.items():
        # Check if key is even then add pair to new dictionary
        if value == 1:
            non_duplicates_dict[key] = key

    print(len(non_duplicates_dict), "non-included addresses found.")
    df_out = pd.DataFrame.from_dict(non_duplicates_dict, orient="index", columns=['address'])
    df_out.to_csv("data/temp/output/test_output.csv",mode="w", header="True", index=None)
    #1062 non-included addresses found
    #5807 + 1062 = 6869 (CORRECT!)

@app.command()
def exit():
    pass

def main():
    app()

if __name__ == "__main__":
    main()
