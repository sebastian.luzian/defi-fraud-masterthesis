import requests
from retrying import retry

BASE_URL = "https://api.etherscan.io"
API_KEY = "R5X49IDTUJE3ME2Y554R6EVCIX6FHTDUYM"

def retry_if_connection_error(exception):
    """ Specify an exception you need. or just True"""
    #return True
    return isinstance(exception, ConnectionError)

# Retrieves all Normal Transactions for an address
# retrieved txs per address is limited to 10k
#e.g. https://api.etherscan.io/api?module=account&action=txlist&address=0xD0cC2B24980CBCCA47EF755Da88B220a82291407&startblock=0&endblock=99999999&sort=asc&apikey=R5X49IDTUJE3ME2Y554R6EVCIX6FHTDUYM
@retry(retry_on_exception=retry_if_connection_error, wait_fixed=2000)
def get_normal_transactions(address, **kwargs):
    return requests.get(BASE_URL + "/api?module=account&action=txlist&address="+str.strip(address)+"&startblock=0&endblock=99999999&sort=asc&apikey="+API_KEY)

# Retrieves all Internal Transactions for an address
# retrieved txs per address is limited to 10k
#e.g. https://api.etherscan.io/api?module=account&action=txlistinternal&address=0xD0cC2B24980CBCCA47EF755Da88B220a82291407&startblock=0&endblock=99999999&sort=asc&apikey=R5X49IDTUJE3ME2Y554R6EVCIX6FHTDUYM
@retry(retry_on_exception=retry_if_connection_error, wait_fixed=2000)
def get_internal_transactions(address, **kwargs):
    return requests.get(BASE_URL + "/api?module=account&action=txlistinternal&address="+str.strip(address)+"&startblock=0&endblock=99999999&sort=asc&apikey="+API_KEY)
