import time
import typer
from typing import List
import api
import pandas as pd
import matplotlib.pyplot as plt
from datetime import datetime

app = typer.Typer()

# Gets all transactions for an address
# Either "normal" or "internal" transactions are supported
def _get_txs_from_address(address: str, tx_type: str = "normal"):
    if tx_type == "normal":
        response = api.get_normal_transactions(address)
    if tx_type == "internal":
        response = api.get_internal_transactions(address)

    transactions = response.json().get("result")
    # only get ingoing transactions
    ingoing_txs = list(filter(lambda transaction: transaction.get("to") == address.lower(), transactions))
    # only get outgoing transactions
    outgoing_txs = list(filter(lambda transaction: transaction.get("from") == address.lower(), transactions))
    # get txs count
    return transactions, ingoing_txs, outgoing_txs

@app.command()
def get_tx_activity_of_fraud_addresses_connected_to_uniswap(input_file: str = "input/fraud_addresses_connected_to_uniswap.csv", recalculate: bool = False):
    df = pd.read_csv(input_file)
    #remove duplicates
    df = df.drop_duplicates(subset=['fraud_addresses_connected_to_uniswap'])

    #uniswap_addresses_df = pd.read_csv("input/uniswap_addresses.csv")
    uniswap_addresses_df = pd.read_csv("input/extracted_uniswap_addresses.csv")

    start_time = time.time()

    outgoing_value_per_date = dict()

    count = 0
    if recalculate:
        for index, row in df.iterrows():
            print(f"{count}/{len(df)}")
            count+=1
            if count == -1: # set > 0 for testing
                break
            address = row["address"]
            # get transactions of fraud address
            transactions, ingoing_txs, outgoing_txs = _get_txs_from_address(address)
            for tx in outgoing_txs:
                # if the recepient is not a uniswap address, skip the transaction
                if not tx.get("to").lower() in uniswap_addresses_df["address"].values:
                    continue
                # Add the transactions value to the blockNumber it occured in
                timestamp = int(tx.get("timeStamp"))
                date = datetime.fromtimestamp(timestamp).strftime("%d.%m.%Y") # Get the date as String
                value = float(tx.get("value")) / 1.0e18  # convert wei to ETH
                outgoing_value_per_date[date] = outgoing_value_per_date.get(date, 0) + value
        df_out = pd.DataFrame.from_dict(outgoing_value_per_date, orient="index", columns=["value"])

        df_out.reset_index(inplace=True) #convert the date index into a column
        df_out = df_out.rename(columns = {'index' : 'date'}) # rename the newly created column to "date"

        #sort df by date
        df_out["date"] = pd.to_datetime(df_out["date"], format="%d.%m.%Y")
        df_out = df_out.sort_values(by="date")
        df_out["sum"] = df_out["value"].cumsum()
    else:
        df_out = pd.read_csv("output/tx_activity.csv")

    print(df_out.tail(10))
    df_out.plot(x="date",y=["value", "sum"])
    plt.show()
    df_out.to_csv("tx_activity.csv",mode="w", header="True", index=None)

@app.command()
def get_tx_activity_of_fraud_addresses_connected_to_defi(input_file: str = "input/fraud_addresses_connected_to_protocols.csv", recalculate: bool = False, skip_to: str = "", n: int = 0):
    df = pd.read_csv(input_file)
    #remove duplicates
    #df = df.drop_duplicates(subset=['fraud_addresses_connected_to_uniswap'])
    #uniswap_addresses_df = pd.read_csv("input/uniswap_addresses.csv")

    protocol_addresses_dict = {}
    fraud_addresses_connected_to_protocol_dict = {}

    protocols = df["da.p_protocol"].unique().tolist()

    p_count = protocols.index(skip_to)+1 if skip_to != "" else 1
    start_index = p_count-1

    print(f"Starting at {skip_to if skip_to != '' else protocols[0] } ({p_count})")

    n = len(protocols) if n == 0 else n

    for protocol in protocols[start_index:start_index+n]:
        print(f"Processing protocol {p_count}/{len(protocols)} ({protocol})")

        protocol_addresses_dict[protocol] = df[df["da.p_protocol"] == protocol]["da.address"].drop_duplicates().tolist()
        fraud_addresses_connected_to_protocol_dict[protocol] = df[df["da.p_protocol"] == protocol]["fa.address"].drop_duplicates().tolist()

        protocol_addresses = protocol_addresses_dict[protocol]
        fraud_addresses = fraud_addresses_connected_to_protocol_dict[protocol]


        start_time = time.time()
        outgoing_value_per_date = dict()
        count = 1
        if recalculate:
            for address in fraud_addresses:
                print(f"{count}/{len(fraud_addresses)}")
                count+=1
                if count == -1: # set > 0 for testing
                    break
                # get transactions of fraud address
                transactions, ingoing_txs, outgoing_txs = _get_txs_from_address(address)
                for tx in outgoing_txs:
                    # if the recepient is not a uniswap address, skip the transaction
                    if not tx.get("to").lower() in protocol_addresses:
                        #print("continue")
                        continue
                    # Add the transactions value to the blockNumber it occured in
                    timestamp = int(tx.get("timeStamp"))
                    date = datetime.fromtimestamp(timestamp).strftime("%d.%m.%Y") # Get the date as String
                    value = float(tx.get("value")) / 1.0e18  # convert wei to ETH
                    outgoing_value_per_date[date] = outgoing_value_per_date.get(date, 0) + value

            df_out = pd.DataFrame.from_dict(outgoing_value_per_date, orient="index", columns=["value"])

            df_out.reset_index(inplace=True) #convert the date index into a column
            df_out = df_out.rename(columns = {'index' : 'date'}) # rename the newly created column to "date"

            #sort df by date
            df_out["date"] = pd.to_datetime(df_out["date"], format="%d.%m.%Y")
            df_out = df_out.sort_values(by="date")
            df_out["sum"] = df_out["value"].cumsum()
        else:
            df_out = pd.read_csv("output/"+protocol+"_tx_activity.csv")

        if(len(df_out) == 1):
            # if df has only one point, add another to be plottable as line
            add_df = pd.DataFrame([[ pd.to_datetime(datetime.now().strftime("%d.%m.%Y"), format="%d.%m.%Y"), 0.0, df_out["sum"][0] ]],columns=['date','value','sum'])
            df_out = pd.concat([df_out, add_df], ignore_index=True)

        df_out["date"] = pd.to_datetime(df_out["date"], format="%Y-%m-%d")
        fig, ax = plt.subplots()
        ax.xaxis_date()
        # Optional. Just rotates x-ticklabels in this case.
        fig.autofmt_xdate()

        #df_out.plot(x="date",y=["value", "sum"])
        #plt.scatter(df_out["date"], df_out["value"], label="Value", s=1, c="red")
        plt.bar(df_out["date"], df_out["value"], label="Value", width=2, color=(1, 0, 0, 0.5))
        #plt.plot(df_out["date"], df_out["sum"], label="Cumulative Sum")
        plt.step(df_out["date"], df_out["sum"], label="Cumulative Sum")

        plt.xlabel('Time')
        # Set the y axis label of the current axis.
        plt.ylabel('Amount in ETH')
        # Set a title of the current axes.
        plt.title('Fraudulent funds flowing to '+protocol)
        # show a legend on the plot
        ax.legend()

        plt.savefig("output/" + protocol + '_tx_activity.png')
        #plt.show()
        plt.close()
        df_out.to_csv("output/"+protocol+"_tx_activity.csv",mode="w", header="True", index=None)
        p_count+=1

@app.command()
def exit():
    pass

def main():
    app()

if __name__ == "__main__":
    main()
