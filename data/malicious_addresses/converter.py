import typer
import pandas as pd
import json
import yaml
import re
import base64


app = typer.Typer()

def _valid(address):
    ethereum_address_pattern = re.compile(r'^(0x[a-fA-F0-9]{40})$')
    base64_address_pattern = re.compile(r'^[A-Za-z0-9+/]{27}=+$')
    if ethereum_address_pattern.match(address):
        return address
    elif base64_address_pattern.match(address):
        return "0x"+base64.b64decode(address).hex()
    else:
        return False


@app.command()
def convert_labelcloud_csv(write_or_append: str = "a", filename: str = "input/labelcloud.csv", converted_filename: str = "output/addresses.csv"):
    # Everything is already in the right format
    output = pd.read_csv(filename)
    output['origin'] = filename
    output.to_csv(converted_filename, mode=write_or_append, header=write_or_append == "w", index=None)
    print("labelcloud.csv: Found ", len(output.index), " addresses.")
    return len(output.index)

@app.command()
def convert_etherscamdb_tagpack_yaml(write_or_append: str = "a", filename: str = "input/etherscamdb_tagpack.yaml", converted_filename: str = "output/addresses.csv"):
    data = yaml.safe_load(open(filename))
    tags = data['tags']
    output = pd.DataFrame()
    for item in tags:
        address = item["address"]
        category = ""
        reporter = "Complexity Science Hub"
        url = item["label"]
        description = ""
        if item["currency"] == "ETH" and _valid(address):
            row = pd.DataFrame({'address': [_valid(address)],
                                'category': [category],
                                'reporter': [reporter],
                                'url': [url],
                                'description': [description],
                                })
            output = pd.concat([output, row], ignore_index=True, axis=0)

    output['origin'] = filename
    output.to_csv(converted_filename, mode=write_or_append, header=write_or_append == "w", index=None)
    print("etherscamdb_tagpack.yaml: Found ", len(output.index), " addresses.")
    return len(output.index)

@app.command()
def convert_scams_yaml(write_or_append: str = "a", filename: str = "input/scams.yaml", converted_filename: str = "output/addresses.csv"):
    data = yaml.safe_load(open(filename))
    output = pd.DataFrame()
    for item in data:
        if 'addresses' in item:
            for adr in item["addresses"]:
                address = adr
                if "category" in item: category = item["category"]
                else: category = ""
                if "reporter" in item: reporter = item["reporter"]
                else: reporter = ""
                if "url" in item: url = item["url"]
                else: url = ""
                if "description" in item: description = item["description"]
                else: description = ""
                if _valid(address):
                    row = pd.DataFrame({'address': [_valid(address)],
                                        'category': [category],
                                        'reporter': [reporter],
                                        'url': [url],
                                        'description': [description],
                                        })
                    output = pd.concat([output, row], ignore_index=True, axis=0)

    output['origin'] = filename
    output.to_csv(converted_filename, mode=write_or_append, header=write_or_append == "w", index=None)
    print("scams.yaml: Found ", len(output.index), " addresses.")
    return len(output.index)

@app.command()
def convert_addresses_darklist_json(write_or_append: str = "a", filename: str = "input/addresses-darklist.json", converted_filename: str = "output/addresses.csv"):
    data = json.load(open(filename))
    output = pd.DataFrame()
    for item in data:
        address = item["address"]
        category = ""
        reporter = "MyEtherWallet"
        url = ""
        description = item["comment"]
        if _valid(address):
            row = pd.DataFrame({'address': [_valid(address)],
                                'category': [category],
                                'reporter': [reporter],
                                'url': [url],
                                'description': [description],
                                })
            output = pd.concat([output, row], ignore_index=True, axis=0)

    output['origin'] = filename
    output.to_csv(converted_filename, mode=write_or_append, header=write_or_append == "w", index=None)
    print("addresses-darklist.json: Found ", len(output.index), " addresses.")
    return len(output.index)

@app.command()
def convert_addresses_json(write_or_append: str = "w", filename: str = "input/addresses.json", converted_filename: str = "output/addresses.csv"):
    data = json.load(open(filename))
    result = data['result']
    output = pd.DataFrame()
    for key in result:
        for item in result[key]:
            address = item["address"]
            category = item["category"]
            reporter = item["reporter"]
            url = item["url"]
            description = item["description"]
            if _valid(address):
                row = pd.DataFrame({'address': [_valid(address)],
                                    'category': [category],
                                    'reporter': [reporter],
                                    'url': [url],
                                    'description': [description],
                                    })
                output = pd.concat([output, row], ignore_index=True, axis=0)

    output['origin'] = filename
    output.to_csv(converted_filename, mode=write_or_append, header=write_or_append == "w", index=None)
    print("addresses.json: Found ", len(output.index), " addresses.")
    return len(output.index)

@app.command()
def convert_urls_yaml(write_or_append: str = "a", filename: str = "input/urls.yaml", converted_filename: str = "output/addresses.csv"):
    data = yaml.safe_load(open(filename))
    output = pd.DataFrame()
    for item in data:
        if 'addresses' in item:
            for address_type in item["addresses"]:
                if address_type == "ETH":
                    for address in item["addresses"]['ETH']:
                        if "category" in item: category = item["category"]
                        else: category = ""
                        if "reporter" in item: reporter = item["reporter"]
                        else: reporter = ""
                        if "url" in item: url = item["url"]
                        else: url = ""
                        if "description" in item: description = item["description"]
                        else: description = ""
                        if _valid(address):
                            row = pd.DataFrame({'address': [_valid(address)],
                                                'category': [category],
                                                'reporter': [reporter],
                                                'url': [url],
                                                'description': [description],
                                                })
                            output = pd.concat([output, row], ignore_index=True, axis=0)

    output['origin'] = filename
    output.to_csv(converted_filename, mode=write_or_append, header=write_or_append == "w", index=None)
    print("urls.yaml: Found ", len(output.index), " addresses.")
    return len(output.index)

@app.command()
def convert_uris_yaml(write_or_append: str = "a", filename: str = "input/uris.yaml", converted_filename: str = "output/addresses.csv"):
    data = yaml.safe_load(open(filename))
    output = pd.DataFrame()
    for item in data:
        if 'addresses' in item:
            for category in item["addresses"]:
                for address in item["addresses"]['ETH']:
                    if "category" in item: category = item["category"]
                    else: category = ""
                    if "reporter" in item: reporter = item["reporter"]
                    else: reporter = ""
                    if "url" in item: url = item["url"]
                    else: url = ""
                    if "description" in item: description = item["description"]
                    else: description = ""
                    if _valid(address):
                        row = pd.DataFrame({'address': [_valid(address)],
                                            'category': [category],
                                            'reporter': [reporter],
                                            'url': [url],
                                            'description': [description],
                                            })
                        output = pd.concat([output, row], ignore_index=True, axis=0)

    output['origin'] = filename
    output.to_csv(converted_filename, mode=write_or_append, header=write_or_append == "w", index=None)
    print("uris.yaml: Found ", len(output.index), " addresses.")
    return len(output.index)

@app.command() #mandatory
def convert_all():
    # we rank the files from reliable to unreliable
    sum = 0
    sum += convert_labelcloud_csv("w")  # 6265 addresses - etherscan.io
    sum += convert_etherscamdb_tagpack_yaml("a")  # 3526 addresses - EtherScamDB
    sum += convert_scams_yaml("a")  # 3251 addresses - EtherScamDB
    sum += convert_addresses_darklist_json("a")  # 715 addresses - MyEtherWallet
    sum += convert_addresses_json("a")  # 5544 addresses - CryptoScamDB
    sum += convert_urls_yaml("a")  # 4805 addresses - CryptoScamDB
    sum += convert_uris_yaml("a")  # 15 addresses - CryptoScamDB

    print(sum, "addresses found in total.")
    # TOTAL: 20181 addresses (not uniqueified)

@app.command() # mandatory
def lowercase():
    df = pd.read_csv("output/addresses.csv")
    df["address"] = df.apply(lambda row: str(row['address']).lower(), axis=1)  # lowercase addresses
    df.to_csv("output/addresses.csv", index=False)
    print(f"Lowercased all malicious addresses to addresses.csv")


@app.command() # mandatory
def uniqueify():
    df = pd.read_csv("output/addresses.csv")
    # reverse the data
    df = df.iloc[::-1]

    key_dict = {}
    for index, row in df.iterrows():
        # overwrites less reliable entry with more reliable entry
        key_dict[row['address']] = row

    print(len(key_dict), "unique addresses found.")
    df_out = pd.DataFrame.from_dict(key_dict, orient="index", columns=['address', 'category', 'reporter', 'url', 'description', 'origin'])
    df_out.to_csv("output/unique_addresses.csv", mode="w", header="True", index=None)
    # 6869 unique addresses found


def main():
    app()

if __name__ == "__main__":
    main()
