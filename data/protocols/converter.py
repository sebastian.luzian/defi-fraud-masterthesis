import os
import typer
import pandas as pd

app = typer.Typer()

@app.command()
def convert(to_file: str = "output/protocols.csv"):
    header = ['Id', 'Label', 'Type', 'Protocol']
    full_data = pd.DataFrame(columns=header)

    for subdir, dirs, files in os.walk("input/protocols_data"):
        for file in files:
            file_data = pd.read_csv(os.path.join(subdir, file), usecols=["Id", "Label"])

            file_data["Id"] = file_data.apply(lambda row : str(row['Id']).lower(), axis = 1) # lowercase addresses
            # add column 'Type' depending on which folder the file is in: assets, derivatives, dex or lending
            file_data.insert(2, 'Type', os.path.join(subdir, file).split("\\")[1])
            # add column 'Protocol' depending on which folder the file is in: badger, convex, fei...
            file_data.insert(3, 'Protocol', os.path.join(subdir, file).split("\\")[2])
            full_data = pd.concat([full_data, file_data], ignore_index = True, axis=0)
    full_data.to_csv(to_file,header=['address', 'label', 'type', 'protocol'], index=False)
    print(f"Printed {len(full_data)} protocol addresses to {to_file}")

@app.command()
def exit():
    pass

def main():
    app()

if __name__ == "__main__":
    main()


    

