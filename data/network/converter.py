import os
import time
import typer
import pandas as pd
import numpy as np
import re
import base64
import time

app = typer.Typer()

def _replace_address(address):
    base64_address_pattern = re.compile(r'^[A-Za-z0-9+/]{27}=+$')
    if base64_address_pattern.match(address):
        return "0x"+base64.b64decode(address).hex()
    else:
        return ""

# for addresses
@app.command() # mandatory
def prepare_addresses():
    filepath = os.path.join(os.path.dirname(__file__), "..", "..", "data/network/input/addresses/decompressed")

    malicious_addresses_filepath = os.path.join(os.path.dirname(__file__), "..", "..", "data/malicious_addresses/output/unique_addresses.csv")
    protocols_filepath = os.path.join(os.path.dirname(__file__), "..", "..", "data/protocols/output/protocols.csv")

    malicious_addresses_df = pd.read_csv(malicious_addresses_filepath)
    protocols_df = pd.read_csv(protocols_filepath)

    malicious_addresses_dict = {}
    protocols_dict = {}

    for index, row in malicious_addresses_df.iterrows():
        malicious_addresses_dict[row['address']] = row
    for index, row in protocols_df.iterrows():
        protocols_dict[row['address']] = row

    total_time = time.time()
    for filename in os.listdir(filepath):
        t0 = time.time()
        if "output" in filename:
            print(f"Processing {filename}...")
            f = os.path.join(filepath, filename)
            # checking if it is a file
            if os.path.isfile(f):
                df = pd.read_csv(f)

                # replace all base64 encoded addresses with the standard address representation
                df['address'] = df.apply(lambda row: _replace_address(row['address']), axis=1)

                # put matching fraudulent addresses and defi addresses into lists
                is_fraud_address_list = [(address in malicious_addresses_dict) for address in df['address']]
                is_protocol_list = [(address in protocols_dict) for address in df['address']]

                #add fraud address properties if it is a fraud address
                fa_category_list = [(malicious_addresses_dict[address]['category'] if address in malicious_addresses_dict else "") for address in df['address']]
                fa_reporter_list = [(malicious_addresses_dict[address]['reporter'] if address in malicious_addresses_dict else "") for address in df['address']]
                fa_url_list = [(malicious_addresses_dict[address]['url'] if address in malicious_addresses_dict else "") for address in df['address']]
                fa_description_list = [(malicious_addresses_dict[address]['description'] if address in malicious_addresses_dict else "") for address in df['address']]
                fa_origin_list = [(malicious_addresses_dict[address]['origin'] if address in malicious_addresses_dict else "") for address in df['address']]

                # add protocol properties if it is a protocol address
                p_label_list = [(protocols_dict[address]['label'] if address in protocols_dict else "") for address in df['address']]
                p_type_list = [(protocols_dict[address]['type'] if address in protocols_dict else "") for address in df['address']]
                p_protocol_list = [(protocols_dict[address]['protocol'] if address in protocols_dict else "") for address in df['address']]

                df["is_fraud_address"] = is_fraud_address_list
                df["is_protocol"] = is_protocol_list

                df['fa_category'] =  fa_category_list
                df['fa_reporter'] = fa_reporter_list
                df['fa_url'] = fa_url_list
                df['fa_description'] = fa_description_list
                df['fa_origin'] = fa_origin_list

                df['p_label'] = p_label_list
                df['p_type'] = p_type_list
                df['p_protocol'] = p_protocol_list

                df.to_csv("output/addresses/"+filename, header=False, index=False)
            print("Processed in", time.time() - t0, "seconds")
    print("Total time spent:", time.time() - total_time, "seconds")

# for address_relations
@app.command() # mandatory
def prepare_address_relations(folder: str = "address_relations"):
    filepath = os.path.join(os.path.dirname(__file__), "..", "..", "data/network/input/" + folder + "/decompressed")
    total_time = time.time()
    for filename in os.listdir(filepath):
        t0 = time.time()
        if "output" in filename:
            print(f"Processing {filename}...")
            f = os.path.join(filepath, filename)
            if os.path.isfile(f):
                df = pd.read_csv(f)
                df['value'] = df['value'].astype(np.float64) / 1.0e18 #convert wei to ETH
                df.to_csv("output/address_relations/"+filename, header=False, index=False)
            print("Processed in", time.time() - t0, "seconds")
    print("Total time spent:", time.time() - total_time, "seconds")

# for address_relations
@app.command() # optional
def check_for_min_max(folder: str = "address_relations"):
    filepath = os.path.join(os.path.dirname(__file__), "..", "..", "data/network/input/" + folder + "/decompressed")
    total_time = time.time()
    for filename in os.listdir(filepath):
        t0 = time.time()
        if "output" in filename:
            print(filename)
            f = os.path.join(filepath, filename)
            if os.path.isfile(f):
                df = pd.read_csv(f, usecols=[2], names=['value'])
                df['value'] = df['value'].astype(np.float64)
                print(np.sum((df['value'] < 0).values.ravel()), "are < 0") # check how many values are < 0
                print(np.sum((df['value'] > 9223372036854775807).values.ravel()), "are > 9223372036854775807") # check how many values are > MAX_INT

def main():
    app()

if __name__ == "__main__":
    main()
