from neo4j import GraphDatabase
import pandas as pd
import time
import os

class DatabaseConnection:

    def __init__(self, uri, user, password):
        self.driver = GraphDatabase.driver(uri, auth=(user, password))
        self.session = self.driver.session(database="defi")

    def close(self):
        self.driver.close()

    def test(self, address):
        with self.session as session:
            res = session.write_transaction(self._calculate_clustering_coefficient_for_fraud_address, address)
            print(res)
            print(res["fa"]["address"])
            print(res["conn_btw_neighbours"])
            print(res["max_conn_btw_neighbours"])
            print(res["clustering_coefficient"])

    def calculate_clustering_coefficient(self, start = 0, end = -1):
        with self.session as session:
            fraud_addresses_df = pd.read_csv("input/network_study_inputs/db_fraud_addresses_sorted_by_neighbours.csv")

            result_df = pd.DataFrame()
            output_file = "output/network_study_outputs/clustering_coefficient.csv"

            if not os.path.exists(output_file):
                with open(output_file, "w") as file:
                    file.write("address,conn_btw_neighbours,max_conn_btw_neighbours,clustering_coefficient\n")

            with open("output/network_study_outputs/clustering_coefficient.csv", "a+") as outputfile:
                for index, row in fraud_addresses_df.iterrows():
                    start_time = time.time()
                    if index < start:
                        continue
                    if index == end:
                        break
                    print(f'{index + 1}/{len(fraud_addresses_df)}: {row["fa.address"]}')
                    address = row["fa.address"]
                    res = session.write_transaction(self._calculate_clustering_coefficient_for_fraud_address, address)

                    row = pd.DataFrame({
                        "address": [res["fa"]["address"]],
                        "conn_btw_neighbours": [res["conn_btw_neighbours"]],
                        "max_conn_btw_neighbours": [res["max_conn_btw_neighbours"]],
                        "clustering_coefficient": [res["clustering_coefficient"]]
                    })
                    outputfile.write(res["fa"]["address"]+","+str(res["conn_btw_neighbours"])+","+str(res["max_conn_btw_neighbours"])+","+str(res["clustering_coefficient"])+"\n")
                    result_df = pd.concat([result_df, row], ignore_index=True, axis=0)
                    print("Processed in", time.time() - start_time, "seconds")

                result_df.to_csv("output/network_study_outputs/clustering_coefficient_"+str(start)+"_"+str(end)+".csv", header=True, index=None)



    @staticmethod
    def _calculate_clustering_coefficient_for_fraud_address(tx, address):
        result = tx.run("MATCH (fa:Address {is_fraud_address: 'True', address: $address}) "
                        "WITH fa, fa.fa_category as category "
                        "OPTIONAL MATCH all_neighbours = ((fa)-[t:TRANSACTIONS]-(n:Address)) WHERE fa <> n "
                        "WITH fa, category, collect(n) as neighbours, COUNT(DISTINCT n) as n_neighbours "
                        "OPTIONAL MATCH connected_neighbours = ((a)-[t]-(b)) "
                        "WHERE b in neighbours and a in neighbours AND a <> b AND a <> fa AND b <> fa "
                        "RETURN fa, COUNT(t)/2 as conn_btw_neighbours, (n_neighbours*(n_neighbours-1)/2.0) as max_conn_btw_neighbours,  (COUNT(t)/2.0)/((n_neighbours*(n_neighbours-1)/2.0)) as clustering_coefficient"
                        , address=address)
        return result.single()

    def _calculate_number_of_fraud_addresses_in_neighbourhood_of_fraud_addresses(tx):
        result = tx.run("MATCH (fa:Address {is_fraud_address: 'True'}) "
                        "WITH fa"
                        "MATCH fa_neighbours = ((fa)-[t:TRANSACTIONS]-(fa:Address {is_fraud_address: 'True'}))"
                        "MATCH a_neighbours = ((fa)-[t:TRANSACTIONS]-(a:Address))"
                        "RETURN fa.address, fa.category, COUNT(a_neighbours), COUNT(fa_neighbours)"
                        )


if __name__ == "__main__":
    db = DatabaseConnection("bolt://localhost:7687", "neo4j", "password")
    db.calculate_clustering_coefficient(0, -1)
    db.close()
