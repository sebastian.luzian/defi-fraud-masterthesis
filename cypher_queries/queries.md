# Overview

After we have followed the steps inside [README.md](README.md) 
we should see the following statistics in our database:

- 148436284 nodes
- 463089050 relationships
- 1816816820 properties
- Peak memory usage: 2.659GiB

We can now execute arbitrary queries on the Neo4J graph database:

## Cypher Queries

### Naming Conventions
- a = any_address
- fa = fraud_address
- da = defi_address
- t = transaction

### Setting Indexes
    CREATE INDEX address_is_defi IF NOT EXISTS FOR (a:Address) ON (a.is_defi_protocol)
    CREATE INDEX address_is_fraud IF NOT EXISTS FOR (a:Address) ON (a.is_fraud_address)
    SHOW INDEXES YIELD *

### Initial Queries
**Return all fraud addresses directly connected to any address**

    MATCH (fa:Address {is_fraud_address: 'True'})-[t:TRANSACTIONS]-(a:Address) RETURN fa,t,a

**Return all fraud addresses directly connected to any defi address**

    MATCH (fa:Address {is_fraud_address: 'True'})-[t:TRANSACTIONS]-(da:Address {is_defi_protocol: 'True'}) RETURN fa,t,da

**Return all fraud addresses directly connected to any fraud address**

    MATCH (fa1:Address {is_fraud_address: 'True'})-[t:TRANSACTIONS]-(fa2:Address {is_fraud_address: 'True'}) RETURN fa1,t,fa2

**Return all defi addresses directly connected to any defi address**

    MATCH (da1:Address {is_defi_protocol: 'True'})-[t:TRANSACTIONS]-(da2:Address {is_defi_protocol: 'True'}) RETURN da1,t,da2


### Heterogenous flows to DeFi (grouped by protocol type & protocol):
<details>
<summary>
1 HOP
</summary>

```
MATCH p=(da:Address {is_defi_protocol:"True"})<-[t1:TRANSACTIONS]-(fa:Address {is_fraud_address: "True"}) RETURN da.p_type, da.p_protocol, sum(t1.value), length(p), count(t1)
```

```
MATCH p=(da:Address {is_defi_protocol:"True"})
<-[t1:TRANSACTIONS]-(fa:Address {is_fraud_address: "True"})
RETURN da.p_type, da.p_protocol, sum(t1.value), length(p), count(t1)
```
</details>

<details>
<summary>
2 HOPS
</summary>

```
MATCH p=(da:Address {is_defi_protocol:"True"})<-[t2:TRANSACTIONS]-(a1:Address)<-[t1:TRANSACTIONS]-(fa:Address {is_fraud_address: "True"}) RETURN da.p_type, da.p_protocol, sum(t2.value), sum(t1.value), length(p), count(t1)
```

```
MATCH p=(da:Address {is_defi_protocol:"True"})
<-[t2:TRANSACTIONS]-(a1:Address)
<-[t1:TRANSACTIONS]-(fa:Address {is_fraud_address: "True"})
RETURN da.p_type, da.p_protocol, sum(t2.value), sum(t1.value), length(p), count(t1)
```
</details>

<details>
<summary>
3 HOPS
</summary>

```
MATCH p=(da:Address {is_defi_protocol:"True"})<-[t3:TRANSACTIONS]-(a2:Address)<-[t2:TRANSACTIONS]-(a1:Address)<-[t1:TRANSACTIONS]-(fa:Address {is_fraud_address: "True"}) RETURN da.p_type, da.p_protocol, sum(t3.value), sum(t2.value), sum(t1.value), length(p), count(t1)
```

```
MATCH p=(da:Address {is_defi_protocol:"True"})
<-[t3:TRANSACTIONS]-(a2:Address)
<-[t2:TRANSACTIONS]-(a1:Address)
<-[t1:TRANSACTIONS]-(fa:Address {is_fraud_address: "True"})
RETURN da.p_type, da.p_protocol, sum(t3.value), sum(t2.value), sum(t1.value), length(p), count(t1)
```
</details>

### Homogenous flows to DeFi (grouped by protocol type & protocol)
<details>
<summary>
1 HOP
</summary>

```
MATCH p=(da:Address {is_defi_protocol:"True"})<-[t1:TRANSACTIONS]-(fa1:Address {is_fraud_address:"True"}) RETURN da.p_type, da.p_protocol, sum(t1.value), length(p), count(t1)
```

```
MATCH p=(da:Address {is_defi_protocol:"True"})
<-[t1:TRANSACTIONS]-(fa1:Address {is_fraud_address:"True"})
RETURN da.p_type, da.p_protocol, sum(t1.value), length(p), count(t1)
```

</details>

<details>
<summary>
2 HOPS
</summary>

```
MATCH p=(da:Address {is_defi_protocol:"True"})<-[t2:TRANSACTIONS]-(fa2:Address {is_fraud_address:"True"})<-[t1:TRANSACTIONS]-(fa1:Address {is_fraud_address:"True"}) RETURN da.p_type, da.p_protocol, sum(t2.value), sum(t1.value), length(p), count(t1)
```

```
MATCH p=(da:Address {is_defi_protocol:"True"})
<-[t2:TRANSACTIONS]-(fa2:Address {is_fraud_address:"True"})
<-[t1:TRANSACTIONS]-(fa1:Address {is_fraud_address:"True"})
RETURN da.p_type, da.p_protocol, sum(t2.value), sum(t1.value), length(p), count(t1)
```

</details>

<details>

<summary>
3 HOPS
</summary>

```
MATCH p=(da:Address {is_defi_protocol:"True"})<-[t3:TRANSACTIONS]-(fa3:Address {is_fraud_address:"True"})<-[t2:TRANSACTIONS]-(fa2:Address {is_fraud_address:"True"})<-[t1:TRANSACTIONS]-(fa1:Address {is_fraud_address:"True"}) RETURN a.p_type, a.p_protocol, sum(t3.value), sum(t2.value), sum(t1.value), length(p), count(t1)
```

```
MATCH p=(da:Address {is_defi_protocol:"True"})
<-[t3:TRANSACTIONS]-(fa3:Address {is_fraud_address:"True"})
<-[t2:TRANSACTIONS]-(fa2:Address {is_fraud_address:"True"})
<-[t1:TRANSACTIONS]-(fa1:Address {is_fraud_address:"True"})
RETURN a.p_type, a.p_protocol, sum(t3.value), sum(t2.value), sum(t1.value), length(p), count(t1)
```
</details>

### Heterogenous flows to DeFi (grouped by fraud category, protocol type & protocol):
<details>
<summary>
1 HOP
</summary>

```
MATCH p=(da:Address {is_defi_protocol:"True"})<-[t1:TRANSACTIONS]-(fa:Address {is_fraud_address: "True"}) RETURN fa.fa_category, sum(t1.value), da.p_type, da.p_protocol
```

```
MATCH p=(da:Address {is_defi_protocol:"True"})
<-[t1:TRANSACTIONS]-(fa:Address {is_fraud_address: "True"})
RETURN fa.fa_category, sum(t1.value), da.p_type, da.p_protocol
```
</details>

<details>
<summary>
2 HOPS
</summary>

```
MATCH (da:Address {is_defi_protocol: 'True'})<-[t2:TRANSACTIONS]-(a1:Address)<-[t1:TRANSACTIONS]-(fa:Address) RETURN fa.fa_category, sum(t1.value), sum(t2.value), da.p_type, da.p_protocol
```

```
MATCH (da:Address {is_defi_protocol: 'True'})
<-[t2:TRANSACTIONS]-(a1:Address)
<-[t1:TRANSACTIONS]-(fa:Address)
RETURN fa.fa_category, sum(t1.value), sum(t2.value), da.p_type, da.p_protocol
```
</details>

<details>

<summary>
3 HOPS
</summary>

```
MATCH (da:Address {is_defi_protocol: 'True'})<-[t3:TRANSACTIONS]-(a2:Address)<-[t2:TRANSACTIONS]-(a1:Address)<-[t1:TRANSACTIONS]-(fa:Address {is_fraud_address: 'True'}) RETURN fa.fa_category, sum(t1.value), sum(t2.value), sum(t3.value), da.p_type, da.p_protocol
```

```
MATCH (da:Address {is_defi_protocol: 'True'})
<-[t3:TRANSACTIONS]-(a2:Address)
<-[t2:TRANSACTIONS]-(a1:Address)
<-[t1:TRANSACTIONS]-(fa:Address {is_fraud_address: 'True'})
RETURN fa.fa_category, sum(t1.value), sum(t2.value), sum(t3.value), da.p_type, da.p_protocol
```
</details>


### Homogenous flows to DeFi (grouped by fraud category, protocol type & protocol)
<details>
<summary>
1 HOP
</summary>

```
MATCH p=(da:Address {is_defi_protocol:"True"})<-[t1:TRANSACTIONS]-(fa1:Address {is_fraud_address: "True"}) RETURN fa1.fa_category, sum(t1.value), da.p_type, da.p_protocol
```

```
MATCH p=(da:Address {is_defi_protocol:"True"})
<-[t1:TRANSACTIONS]-(fa1:Address {is_fraud_address: "True"})
RETURN fa1.fa_category, sum(t1.value), da.p_type, da.p_protocol
```

</details>

<details>
<summary>
2 HOPS
</summary>

```
MATCH (da:Address {is_defi_protocol: 'True'})<-[t2:TRANSACTIONS]-(fa2:Address {is_fraud_address: 'True'})<-[t1:TRANSACTIONS]-(fa1:Address {is_fraud_address: 'True'}) RETURN fa1.fa_category, sum(t1.value), sum(t2.value), da.p_type, da.p_protocol
```

```
MATCH (da:Address {is_defi_protocol: 'True'})
<-[t2:TRANSACTIONS]-(fa2:Address {is_fraud_address: 'True'})
<-[t1:TRANSACTIONS]-(fa1:Address {is_fraud_address: 'True'})
RETURN fa1.fa_category, sum(t1.value), sum(t2.value), da.p_type, da.p_protocol
```

</details>

<details>
<summary>
3 HOPS
</summary>

```
MATCH (da:Address {is_defi_protocol: 'True'})<-[t3:TRANSACTIONS]-(fa3:Address {is_fraud_address: 'True'})<-[t2:TRANSACTIONS]-(fa2:Address {is_fraud_address: 'True'})<-[t1:TRANSACTIONS]-(fa1:Address {is_fraud_address: 'True'}) RETURN fa1.fa_category, sum(t1.value), sum(t2.value), sum(t3.value), da.p_type, da.p_protocol
```

```
MATCH (da:Address {is_defi_protocol: 'True'})
<-[t3:TRANSACTIONS]-(fa3:Address {is_fraud_address: 'True'})
<-[t2:TRANSACTIONS]-(fa2:Address {is_fraud_address: 'True'})
<-[t1:TRANSACTIONS]-(fa1:Address {is_fraud_address: 'True'})
RETURN fa1.fa_category, sum(t1.value), sum(t2.value), sum(t3.value), da.p_type, da.p_protocol

```
</details>

### 1 HOP transaction value from fraudulent addresses to DeFi addresses by fa_category, da.p_type and da.protocol_type

```
MATCH (:Address {is_defi_protocol: 'True'})
<-[t1:TRANSACTIONS]-(fa:Address {is_fraud_address: 'True'})
WITH COUNT(*) AS total
MATCH (da:Address {is_defi_protocol: 'True'})
<-[t1:TRANSACTIONS]-(fa:Address {is_fraud_address: 'True'})
RETURN fa.fa_category, da.p_type, da.p_protocol, SUM(t1.value), COUNT(t1) as outgoing_txs, ROUND(100 * (TOFLOAT(COUNT(fa)) / TOFLOAT(total)),2)+"" AS percentage
```

### Outgoing transactions from fraudulent entities to any addresses

```
MATCH (:Address)
<-[t1:TRANSACTIONS]-(fa:Address {is_fraud_address: 'True'})
WITH COUNT(*) AS total
MATCH (:Address)
<-[t1:TRANSACTIONS]-(fa:Address {is_fraud_address: 'True'})
RETURN fa.fa_category, SUM(t1.value), COUNT(t1) as outgoing_txs, ROUND(100 * (TOFLOAT(COUNT(fa)) / TOFLOAT(total)),2)+"" AS percentage
```


### Outgoing transactions from fraudulent entities to addresses not marked as fraudulent

```
MATCH (:Address {is_fraud_address: 'False'})
<-[t1:TRANSACTIONS]-(fa:Address {is_fraud_address: 'True'})
WITH COUNT(*) AS total
MATCH (:Address {is_fraud_address: 'False'})
<-[t1:TRANSACTIONS]-(fa:Address {is_fraud_address: 'True'})
RETURN fa.fa_category, SUM(t1.value), COUNT(t1) as outgoing_txs, ROUND(100 * (TOFLOAT(COUNT(fa)) / TOFLOAT(total)),2)+"" AS percentage
```


### Number of fraud addresses by category

```
MATCH (:Address {is_fraud_address: 'True'})
WITH COUNT(*) AS total
MATCH (fa:Address {is_fraud_address: 'True'})
RETURN fa.fa_category, COUNT(fa) as num_fraud_addresses, ROUND(100 * (TOFLOAT(COUNT(fa)) / TOFLOAT(total)),2)+"" AS category_percent```
```

### Extract Addresses Connected to DeFi Protocols
**Extract all:**
```
MATCH p=(da:Address {is_defi_protocol:"True"})<-[t1:TRANSACTIONS]-(fa:Address {is_fraud_address: "True"}) RETURN da.address, da.p_label, da.p_type, da.p_protocol, fa.address, fa.fa_category, t1.value+"" as tx_value
```

**Extract only for effected protocols:**

<details>
<summary>dydx (derivatives)</summary>

```
MATCH p=(da:Address {is_defi_protocol:"True", p_protocol: "dydx" })<-[t1:TRANSACTIONS]-(fa:Address {is_fraud_address: "True"}) RETURN da.address, da.p_label, da.p_type, da.p_protocol, fa.address, fa.fa_category, t1.value+"" as tx_value
```

</details>

<details>
<summary>0x (dex)</summary>

```
MATCH p=(da:Address {is_defi_protocol:"True", p_protocol: "0x" })<-[t1:TRANSACTIONS]-(fa:Address {is_fraud_address: "True"}) RETURN da.address, da.p_label, da.p_type, da.p_protocol, fa.address, fa.fa_category, t1.value+"" as tx_value
```

</details>

<details>
<summary>1inch (dex)</summary>

```
MATCH p=(da:Address {is_defi_protocol:"True", p_protocol: "1inch" })<-[t1:TRANSACTIONS]-(fa:Address {is_fraud_address: "True"}) RETURN da.address, da.p_label, da.p_type, da.p_protocol, fa.address, fa.fa_category, t1.value+"" as tx_value
```

</details>

<details>
<summary>balancer (dex)</summary>

```
MATCH p=(da:Address {is_defi_protocol:"True", p_protocol: "balancer" })<-[t1:TRANSACTIONS]-(fa:Address {is_fraud_address: "True"}) RETURN da.address, da.p_label, da.p_type, da.p_protocol, fa.address, fa.fa_category, t1.value+"" as tx_value
```

</details>

<details>
<summary>curvefinance (dex)</summary>

```
MATCH p=(da:Address {is_defi_protocol:"True", p_protocol: "curvefinance" })<-[t1:TRANSACTIONS]-(fa:Address {is_fraud_address: "True"}) RETURN da.address, da.p_label, da.p_type, da.p_protocol, fa.address, fa.fa_category, t1.value+"" as tx_value
```

</details>

<details>
<summary>sushiswap (dex)</summary>

```
MATCH p=(da:Address {is_defi_protocol:"True", p_protocol: "sushiswap" })<-[t1:TRANSACTIONS]-(fa:Address {is_fraud_address: "True"}) RETURN da.address, da.p_label, da.p_type, da.p_protocol, fa.address, fa.fa_category, t1.value+"" as tx_value
```

</details>

<details>
<summary>uniswap (dex)</summary>

```
MATCH p=(da:Address {is_defi_protocol:"True", p_protocol: "uniswap" })<-[t1:TRANSACTIONS]-(fa:Address {is_fraud_address: "True"}) RETURN da.address, da.p_label, da.p_type, da.p_protocol, fa.address, fa.fa_category, t1.value+"" as tx_value
```

</details>

<details>
<summary>compound (lending)</summary>

```
MATCH p=(da:Address {is_defi_protocol:"True", p_protocol: "compound" })<-[t1:TRANSACTIONS]-(fa:Address {is_fraud_address: "True"}) RETURN da.address, da.p_label, da.p_type, da.p_protocol, fa.address, fa.fa_category, t1.value+"" as tx_value

```

</details>



##Additional Queries


### Getting the amount of fraud addresses (grouped by category & with percentages)
    MATCH(fa:Address {is_fraud_address: 'True'}) RETURN COUNT(fa)

    MATCH(fa:Address {is_fraud_address: 'True'}) RETURN fa.fa_category, COUNT(fa)

    MATCH(:Address {is_fraud_address: 'True'})
    WITH COUNT(*) as total
    MATCH (fa:Address {is_fraud_address: 'True'})
    RETURN fa.fa_category, COUNT(fa) as c, 
    ROUND(100 * (TOFLOAT(COUNT(fa)) / TOFLOAT(total)),2) AS percentage, total ORDER BY c DESC

### Getting the amount of DeFi addresses (grouped by protocol type)
    MATCH(da:Address {is_defi_protocol: 'True'}) RETURN COUNT(da)

    MATCH(da:Address {is_defi_protocol: 'True'}) RETURN da.d_type, COUNT(da)






