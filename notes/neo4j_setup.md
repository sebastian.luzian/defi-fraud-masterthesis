# Neo4J Locations:
# C:\Users\USERNAME\.Neo4jDesktop\relate-data\dbmss\dbms-0a6a1b21-1c92-4784-9582-01b9ed5dd0c5\import - Import Folder
# C:\Users\USERNAME\.Neo4jDesktop\relate-data\dbmss\dbms-0a6a1b21-1c92-4784-9582-01b9ed5dd0c5\bin - Neo4J Admin Tool
# Note: the hash location changes whenever a new DBMS is created

##################################################################################
###                  Steps to import data with neo4j-admin                     ###
##################################################################################
# 0) Create a new Project
# 1) Add DBMS - Local DBMS with password "password"
# 2) Database we want to create must not exist yet
# 3) Stop the DBMS
# 4) Run the command from below in the Neo4J Admin Tool location (in one line - https://lingojam.com/TexttoOneLine)
# 5) Start the DBMS
# 6) Inside Neo4J Browser run ":use system" and "create database defi"
# 6) Stop the DBMS
# 7) Start the DBMS
# 8) 

"""
neo4j-admin import --database defi --nodes=Address="
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\addresses\header.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\addresses\output-000001.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\addresses\output-000002.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\addresses\output-000003.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\addresses\output-000004.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\addresses\output-000005.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\addresses\output-000006.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\addresses\output-000007.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\addresses\output-000008.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\addresses\output-000009.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\addresses\output-000010.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\addresses\output-000011.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\addresses\output-000012.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\addresses\output-000013.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\addresses\output-000014.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\addresses\output-000015.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\addresses\output-000016.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\addresses\output-000017.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\addresses\output-000018.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\addresses\output-000019.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\addresses\output-000020.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\addresses\output-000021.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\addresses\output-000022.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\addresses\output-000023.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\addresses\output-000024.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\addresses\output-000025.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\addresses\output-000026.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\addresses\output-000027.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\addresses\output-000028.csv
" --relationships=TRANSACTIONS="
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\address_relations\header.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\address_relations\output-000001.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\address_relations\output-000002.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\address_relations\output-000003.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\address_relations\output-000004.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\address_relations\output-000005.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\address_relations\output-000006.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\address_relations\output-000007.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\address_relations\output-000008.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\address_relations\output-000009.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\address_relations\output-000010.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\address_relations\output-000011.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\address_relations\output-000012.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\address_relations\output-000013.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\address_relations\output-000014.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\address_relations\output-000015.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\address_relations\output-000016.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\address_relations\output-000017.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\address_relations\output-000018.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\address_relations\output-000019.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\address_relations\output-000020.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\address_relations\output-000021.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\address_relations\output-000022.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\address_relations\output-000023.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\address_relations\output-000024.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\address_relations\output-000025.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\address_relations\output-000026.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\address_relations\output-000027.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\network\output\address_relations\output-000028.csv
"
"""


ONE HOP Network:

"""
neo4j-admin import --database onehop --nodes=Address="
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\one_hop_network\output\addresses\header.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\one_hop_network\output\addresses\neighbours.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\one_hop_network\output\addresses\defi_addresses.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\one_hop_network\output\addresses\fraud_addresses.csv
" --relationships=TRANSACTIONS="
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\one_hop_network\output\address_relations\header.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\one_hop_network\output\address_relations\transactions.csv
"
"""


UNISWAP TRACES:

"""
neo4j-admin import --database traces --nodes=Address="
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\traces\output\addresses\enriched\header.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\traces\output\addresses\enriched\all_with_null.csv
" --relationships=TRANSACTIONS="
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\traces\output\malicious_traces\header.csv,
C:\Users\USERNAME\Documents\Informatik\defi-fraud-masterthesis\data\traces\output\malicious_traces\malicious_traces_checked.csv
"
"""